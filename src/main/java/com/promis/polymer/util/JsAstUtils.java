/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, 
 * v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.promis.polymer.util;

import java.util.List;

import org.eclipse.wst.jsdt.core.dom.ASTNode;
import org.eclipse.wst.jsdt.core.dom.ASTVisitor;
import org.eclipse.wst.jsdt.core.dom.Comment;
import org.eclipse.wst.jsdt.core.dom.FunctionInvocation;
import org.eclipse.wst.jsdt.core.dom.JavaScriptUnit;
import org.eclipse.wst.jsdt.core.dom.ObjectLiteral;
import org.eclipse.wst.jsdt.core.dom.ObjectLiteralField;
import org.eclipse.wst.jsdt.core.dom.SimpleName;
import org.eclipse.wst.jsdt.core.dom.StringLiteral;

public class JsAstUtils
{
	public static class FindInvocationVisitor extends ASTVisitor
	{
		public static class Found extends RuntimeException
		{
			private final FunctionInvocation node;

			public Found(FunctionInvocation node)
			{
				this.node = node;
			}
			
			public FunctionInvocation getNode()
			{
				return node;
			}
		}
		
		private final String name;
		
		public FindInvocationVisitor(String name)
		{
			this.name = name;
		}
		
		@Override
		public boolean visit(FunctionInvocation node)
		{
			if (name.equals(getAstString(node.getName())))
				throw new Found(node);
			return true;
		}
	}
	
	private JsAstUtils() {}

	public static String getAstString(Object value)
	{
		if (value == null)
			return null;
		if (value instanceof StringLiteral)
			return ((StringLiteral)value).getLiteralValue();
		if (value instanceof SimpleName)
			return ((SimpleName)value).getIdentifier();
		return value.toString();
	}

	public static ObjectLiteralField findAstField(ObjectLiteral obj, String name)
	{
		@SuppressWarnings("unchecked")
		List<ObjectLiteralField> fields = obj.fields();
		for (ObjectLiteralField field : fields)
		{
			if (name.equals(getAstString(field.getFieldName())))
				return field;
		}
		return null;
	}

	public static Comment findAstComment(JavaScriptUnit jsUnit, ASTNode node)
	{
		int commentIndex = jsUnit.firstLeadingCommentIndex(node);
		if (commentIndex >= 0)
		{
			Comment c = (Comment)jsUnit.getCommentList().get(commentIndex);
			return c; 
		}
		return null;
	}

	public static String findAstDocComment(JavaScriptUnit jsUnit, ASTNode node)
	{
		Comment c = findAstComment(jsUnit, node);
		if (c != null && c.isDocComment())
			return c.toString();
		return null;
	}
}