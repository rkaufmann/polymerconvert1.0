# PolymerConvert
Polymer convert is a simple converter of Polymer elements to GWT wrapper classes.

### Usage
`gradle shadowJar`

`java -jar build\libs\PolymerConvert-0.1-all.jar [-p package.name][-d] 'file/dir names'`

Switches:

 * `-d, --dir` switches to directory mode (all files in given directory are scanned, new Java file is created for each file, non-recursive), if not set only one file is scanned (and stdout used as output).
 * `-p, --package` Specify package of the output Java files (defaults to `org.polymer.elements`)

If you don't like the output format, use your IDE to reformat the code.

### Features (what is converted):
 * Properties
 * Methods (parameter-less)
 * Mixins
 * Comments
 * Events (names only, with comments)
 * Type guessing

### TODO
 * More cleanup
 * Polymer 0.8
 * `JsType`
 * Better comment formatting

### Thanks to
 * Eclipse project (JSDT team for the JS AST)
 * [jsoup](http://jsoup.org) (for the HTML parser)
 * [jansi](https://github.com/fusesource/jansi)
 * [Apache Commons CLI](https://commons.apache.org/proper/commons-cli/)

# License
[MPL 2.0](https://www.mozilla.org/MPL/2.0/)